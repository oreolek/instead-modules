# INSTEAD modules

A collection of different modules for [INSTEAD](http://instead.syscall.ru) adventure game engine.

* [English guide](https://bitbucket.org/oreolek/instead-modules/wiki/Guide.md)
* [Русское руководство](https://bitbucket.org/oreolek/instead-modules/wiki/%D0%98%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B5)
